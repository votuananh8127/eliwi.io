import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'
import Menu from './Menu';
import Slide from './Slide';


const Header = () => (
        <div>
        <header id="header" className="header-transparent header-effect-shrink appear-animation" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}" data-appear-animation="fadeIn" data-appear-animation-delay="200">
        <div className="header-body border-top-0 header-body-bottom-border">
        <div className="header-container container">
                        <div className="header-row">
                                <div className="header-column">
                                        <div className="header-row">
                                                <div className="header-logo">
                                                <Link to='/'>
                                                                <img alt="Porto" width="100" height="48" data-sticky-width="82" data-sticky-height="40" src="images/logo-flat.png"/>
                                                </Link>
                                                </div>
                                        </div>
                                </div>
                                <Menu />
                        </div>
                </div>  
        </div>
        </header>		
        </div>

)

export default Header