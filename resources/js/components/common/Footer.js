import axios from 'axios'
import React , { Component } from 'react';

class Manufacturer extends Component {
    render () {
        return (
			
<footer id="footer" className="mt-0">
				<div className="container my-4">
					<div className="row py-5">
						<div className="col-lg-2 align-self-center mb-5 mb-lg-0">
							<a href="demo-seo.html">
								<img src="images/logo-default-slim.png" className="img-fluid" alt="Demo SEO" width="100" />
							</a>
						</div>
						<div className="col-lg-3 mb-4 mb-lg-0">
							<h4 className="font-weight-normal text-color-light text-5 ls-0 mb-4">Contact Details</h4>
							<ul className="list list-unstyled">
								<li className="d-flex align-items-baseline"><i className="far fa-dot-circle text-color-primary mr-3"></i><div><span className="text-color-light">Address:</span> 1234 Street Name, City Name, USA</div></li>
								<li className="d-flex align-items-baseline"><i className="fab fa-whatsapp text-color-primary mr-3"></i><div><span className="text-color-light">Phone:</span> (123) 456-7890</div></li>
								<li className="d-flex align-items-baseline"><i className="far fa-envelope text-color-primary mr-3"></i><div><span className="text-color-light">Email:</span> <a href="mailto:mail@example.com">mail@example.com</a></div></li>
							</ul>
						</div>
						<div className="col-lg-3 mb-4 mb-lg-0">
							<h4 className="font-weight-normal text-color-light text-5 ls-0 mb-4">Navigations</h4>
							<ul className="list list-unstyled">
								<li className="mb-1">
									<a className="link-hover-style-1 active" data-hash data-hash-offset="95" href="#home">Home</a>
								</li>
								<li className="mb-1">
									<a className="link-hover-style-1"  data-hash data-hash-offset="95" href="#about">About</a>
								</li>
								<li className="mb-1">
									<a className="link-hover-style-1"  data-hash data-hash-offset="95" href="#solutions">Solutions</a>
								</li>
								<li className="mb-1">
									<a className="link-hover-style-1"  data-hash data-hash-offset="95" href="#pricing">Pricing</a>
								</li>
								<li className="mb-1">
									<a className="link-hover-style-1"  data-hash data-hash-offset="95" href="#reviews">Reviews</a>
								</li>
								<li className="mb-1">
									<a className="link-hover-style-1"  data-hash data-hash-offset="95" href="#news">News</a>
								</li>
							</ul>
						</div>
						<div className="col-lg-4">
							<h4 className="font-weight-normal text-color-light text-5 ls-0 mb-4">Newsletter</h4>
							<p>Enter your email address and get our offers and plans in to your mail.</p>
							<div className="alert alert-success d-none" id="newsletterSuccess">
								<strong>Success!</strong> You've been added to our email list.
							</div>
							<div className="alert alert-danger d-none" id="newsletterError"></div>
							<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST" className="mw-100">
								<div className="input-group">
									<input className="form-control form-control-sm text-color-light custom-bg-color-1 rounded-0 border-0 px-4 text-3" placeholder="Email Address..." name="newsletterEmail" id="newsletterEmail" type="text"/>
									<span className="input-group-append">
										<button className="btn btn-primary text-color-light text-2 rounded-0 py-3 px-4" type="submit"><strong>SUBSCRIBE</strong></button>
									</span>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div className="footer-copyright footer-copyright-style-2">
					<div className="container py-2">
						<div className="row justify-content-between py-4">
							<div className="col-auto">
								<p>© Copyright 2019. All Rights Reserved.</p>
							</div>
							<div className="col-auto">
								<ul className="footer-social-icons social-icons social-icons-clean social-icons-icon-light ml-3">
									<li className="social-icons-facebook"><a href="" target="_blank" title="Facebook"><i className="fab fa-facebook-f"></i></a></li>
									<li className="social-icons-twitter"><a href="" target="_blank" title="Twitter"><i className="fab fa-twitter"></i></a></li>
									<li className="social-icons-linkedin"><a href="" target="_blank" title="Linkedin"><i className="fab fa-linkedin-in"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>
			
			
        )
    }
}
export default Manufacturer