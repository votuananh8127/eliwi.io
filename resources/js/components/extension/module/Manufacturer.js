import axios from 'axios'
import React , { Component } from 'react';

class Manufacturer extends Component {
    render () {
        return (
			
			
<section className="section border-0 m-0">
					<div className="container">
						<div className="row">
							<div className="col">
								<div className="owl-carousel owl-theme carousel-center-active-item mb-0" data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 5}, '1200': {'items': 5}}, 'autoplay': true, 'autoplayTimeout': 3000, 'stagePadding': 0, 'dots': false, 'nav': false}">
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-1.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-2.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-3.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-4.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-5.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-6.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-4.png" alt=""/>
									</div>
									<div className="px-3">
										<img className="img-fluid" src="images/logos/logo-2.png" alt=""/>
									</div>
								</div>
							</div>
						</div>
						</div>
						</section>
			
			
        )
    }
}
export default Manufacturer