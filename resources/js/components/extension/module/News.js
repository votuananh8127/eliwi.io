import axios from 'axios'
import React , { Component } from 'react';

class News extends Component {
    render () {
        return (
			
			<div className="container py-4 my-5" id="news">
			<div className="row">
				<div className="col text-center">
					<h2 className="font-weight-bold line-height-2 text-7 mb-1">News</h2>
					<span className="d-block text-color-dark text-5 pb-2 mb-4 opacity-7">Our Blog</span>
				</div>
			</div>
			<div className="row">
				<div className="col-md-4 mb-5 mb-md-0">
					<article>
						<a href="demo-seo-blog-post.html" className="text-decoration-none">
							<span className="thumb-info thumb-info-no-borders custom-thumb-info-style-1 pb-2 mb-2 mb-md-4">
								<span className="thumb-info-wrapper">
									<img src="images/demos/seo/blog/blog-1.jpg" className="img-fluid" alt=""/>
									<span className="custom-thumb-info-date text-color-light bg-primary">
										28
										<span>APR</span>
									</span>
								</span>
							</span>
						</a>
						<h2 className="font-weight-semibold custom-fs-1 line-height-3 ls-0 mb-2"><a href="demo-seo-blog-post.html" className="text-color-dark text-decoration-none">You can now automatically set your browser history to be deleted</a></h2>
						<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis, tortor vel euismod elementum, sapien risus...</p>
					</article>
				</div>
				<div className="col-md-4 mb-5 mb-md-0">
					<article>
						<a href="demo-seo-blog-post.html" className="text-decoration-none">
							<span className="thumb-info thumb-info-no-borders custom-thumb-info-style-1 pb-2 mb-2 mb-md-4">
								<span className="thumb-info-wrapper">
									<img src="images/demos/seo/blog/blog-2.jpg" className="img-fluid" alt=""/>
									<span className="custom-thumb-info-date text-color-light bg-primary">
										22
										<span>APR</span>
									</span>
								</span>
							</span>
						</a>
						<h2 className="font-weight-semibold custom-fs-1 line-height-3 ls-0 mb-2"><a href="demo-seo-blog-post.html" className="text-color-dark text-decoration-none">10 SEO best practices you should be doing right now</a></h2>
						<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis, tortor vel euismod elementum, sapien risus...</p>
					</article>
				</div>
				<div className="col-md-4">
					<article>
						<a href="demo-seo-blog-post.html" className="text-decoration-none">
							<span className="thumb-info thumb-info-no-borders custom-thumb-info-style-1 pb-2 mb-2 mb-md-4">
								<span className="thumb-info-wrapper">
									<img src="images/demos/seo/blog/blog-3.jpg" className="img-fluid" alt=""/>
									<span className="custom-thumb-info-date text-color-light bg-primary">
										20
										<span>APR</span>
									</span>
								</span>
							</span>
						</a>
						<h2 className="font-weight-semibold custom-fs-1 line-height-3 ls-0 mb-2"><a href="demo-seo-blog-post.html" className="text-color-dark text-decoration-none">Data, Data Everywhere — Where are the Campaign Insights?</a></h2>
						<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi facilisis, tortor vel euismod elementum, sapien risus...</p>
					</article>
				</div>
			</div>
			<div className="row mt-5">
				<div className="col text-center">
					<a href="demo-seo-blog.html" className="btn btn-primary btn-outline btn-rounded font-weight-semibold text-4 btn-px-5 btn-py-2">View More</a>
				</div>
			</div>
		</div>

			
			
        )
    }
}
export default News