import axios from 'axios'
import React , { Component } from 'react';

class Portfolio extends Component {
    render () {
        return (
            <div className="section back-dark2">
						<div className="section">

							<div className="clear"></div>

							<div className="section">
								<div id="portfolio-filter" className="portfolio-filter">
									<ul id="filter">
										<li><a href="#" className="current" data-filter="*" title="">all</a></li>
										<li className="tipped" data-title="3 projects" data-tipper-options='{"direction":"top","follow":"true"}'><a href="#" data-filter=".branding" title="">branding</a></li>
										<li className="tipped" data-title="3 projects" data-tipper-options='{"direction":"top","follow":"true"}'><a href="#" data-filter=".photo" title="">photo</a></li>
										<li className="tipped" data-title="4 projects" data-tipper-options='{"direction":"top","follow":"true"}'><a href="#" data-filter=".motion" title="">motion</a></li>
									</ul>
								</div>

								<div id="projects-grid">
									<a href="project.html">
										<div className="portfolio-box-1 branding">
											<div className="mask"></div>
											<h3><span>branding</span><br />project name</h3>
											<img src="images/portfolio/1.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 photo">
											<div className="mask"></div>
											<h3><span>photo</span><br />project name</h3>
											<img src="images/portfolio/2.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 motion">
											<div className="mask"></div>
											<h3><span>motion</span><br />project name</h3>
											<img src="images/portfolio/3.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 branding">
											<div className="mask"></div>
											<h3><span>branding</span><br />project name</h3>
											<img src="images/portfolio/4.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 photo">
											<div className="mask"></div>
											<h3><span>photo</span><br />project name</h3>
											<img src="images/portfolio/5.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 motion">
											<div className="mask"></div>
											<h3><span>motion</span><br />project name</h3>
											<img src="images/portfolio/6.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 motion">
											<div className="mask"></div>
											<h3><span>motion</span><br />project name</h3>
											<img src="images/portfolio/7.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 branding">
											<div className="mask"></div>
											<h3><span>branding</span><br />project name</h3>
											<img src="images/portfolio/8.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 motion">
											<div className="mask"></div>
											<h3><span>motion</span><br />project name</h3>
											<img src="images/portfolio/9.jpg" alt="" />
										</div>
									</a>
									<a href="project.html">
										<div className="portfolio-box-1 photo">
											<div className="mask"></div>
											<h3><span>photo</span><br />project name</h3>
											<img src="images/portfolio/10.jpg" alt="" />
										</div>
									</a>
								</div>
							</div>


						</div>
						<div className="section back-black padding-top-bottom-small">
							<div className="container z-bigger">
								<div className="twelve columns">
									<div id="owl-logos" className="owl-carousel owl-theme">
										<div className="item">
											<img src="images/logos/white1.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white2.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white3.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white4.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white5.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white6.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white1.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white2.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white3.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white4.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white5.png" alt="" />
										</div>
										<div className="item">
											<img src="images/logos/white6.png" alt="" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
        )
    }
}
export default Portfolio