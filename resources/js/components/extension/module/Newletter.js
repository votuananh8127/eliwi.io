import axios from 'axios'
import React , { Component } from 'react';

class Newletter extends Component {
    render () {
        return (
            <section className="section bg-primary border-0 m-0">
            <div className="container">
                <div className="row align-items-center justify-content-between">
                    <div className="col-lg-12 col-xl-3 mb-3 mb-xl-0">
                        <h2 className="font-weight-semibold text-color-light text-8 text-lg-7 text-xl-6 line-height-2 mb-0">Are you ready to grow your business?</h2>
                    </div>
                    <div className="col-auto mb-3 mb-lg-0">
                        <div className="feature-box feature-box-style-2 align-items-center">
                            <div className="feature-box-icon">
                                <i className="fab fa-whatsapp text-color-light text-9 mr-3 p-relative text-color-dark"></i>
                            </div>
                            <div className="feature-box-info pl-1">
                                <label className="d-block text-color-light line-height-2 opacity-8 mb-1">CALL US NOW</label>
                                <strong className="font-weight-normal text-6"><a href="tel:8001234567" className="text-color-light">(800) 123-4567</a></strong>
                            </div>
                        </div>
                    </div>
                    <div className="col-auto mb-4 mb-lg-0">
                        <div className="feature-box feature-box-style-2 align-items-center">
                            <div className="feature-box-icon">
                                <i className="far fa-envelope text-color-light text-9 mr-3 p-relative text-color-dark"></i>
                            </div>
                            <div className="feature-box-info pl-1">
                                <label className="d-block text-color-light line-height-2 opacity-8 mb-1">SEND US AN EMAIL</label>
                                <strong className="font-weight-normal text-6"><a href="mailto:mail@example.com" className="text-color-light">mail@example.com</a></strong>
                            </div>
                        </div>
                    </div>
                    <div className="col-auto">
                        <a href="#demo-form" className="btn btn-light btn-rounded popup-with-form text-color-primary font-weight-semibold text-4 btn-px-5 py-3">Request a Quote</a>
                    </div>
                </div>
            </div>
        </section>
        )
    }
}
export default Newletter