import axios from 'axios'
import React , { Component } from 'react';

class Abouts extends Component {
    render () {
        return (
            <div className="container py-5 my-4" id="about">
					<div className="row align-items-center">
						<div className="col-lg-5 mb-4 mb-md-5 mb-lg-0">
							<div className="featured-boxes featured-boxes-modern-style-1 custom-image-position-1 d-none d-lg-block appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="200" data-plugin-options="{'accY': -400}">
								<div className="featured-box py-3">
									<div className="featured-box-background" style={{ backgroundImage: 'url("images/demos/seo/generic-1.jpg")' }}></div>
									<div className="box-content px-lg-4 px-xl-5 py-lg-5 m-5"></div>
								</div>
							</div>
							<div className="featured-boxes featured-boxes-modern-style-1 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="400" data-plugin-options="{'accY': -400}">
								<div className="featured-box py-5">
									<div className="featured-box-background" style={{ backgroundImage: 'url("images/demos/seo/generic-2.jpg")' }}></div>
									<div className="box-content px-lg-4 px-xl-5 py-lg-5 m-5">
										<a className="text-decoration-none lightbox" href="https://vimeo.com/45830194" data-plugin-options="{'type':'iframe'}">
											<i className="fas fa-play featured-icon featured-icon-style-2 featured-icon-hover-effect-1 text-color-dark bg-light right-4 top-0 m-0 appear-animation" data-appear-animation="expandIn" data-appear-animation-delay="1200"></i>
										</a>
									</div>
								</div>
							</div>
						</div>
						<div className="col-lg-7 pl-lg-5">
							<div className="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">
								<h2 className="font-weight-bold line-height-2 text-7 mb-1">About</h2>
								<span className="d-block text-color-dark text-5 pb-2 mb-4 opacity-7">Who We Are</span>
							</div>
							<p className="pb-3 mb-4 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesettig.</p>
							<a data-hash data-hash-offset="95" href="#solutions" className="btn btn-primary btn-outline btn-rounded font-weight-semibold text-3 btn-px-5 btn-py-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">Our Solutions</a>
						</div>
					</div>
				</div>
        )
    }
}
export default Abouts