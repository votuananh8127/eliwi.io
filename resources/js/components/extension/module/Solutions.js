import axios from 'axios'
import React , { Component } from 'react';

class Solutions extends Component {
    render () {
        return (
			

			<div className="container py-5 mb-5" id="solutions">
					<div className="row">
							<div className="col text-center">
									<h2 className="font-weight-bold line-height-2 text-7 mb-1">Solutions</h2>
									<span className="d-block text-color-dark text-5 pb-2 mb-4 opacity-7">Services</span>
							</div>
					</div>
					<div className="content-grid">
							<div className="row content-grid-row text-center">
									<div className="col-md-6 col-lg-4 content-grid-item flex-column p-5">
											<img src="images/demos/seo/icons/icon-1.png" className="img-fluid mb-4" alt="" />
											<h3 className="font-weight-semibold text-transform-none text-5 mb-3">Web design</h3>
											<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor vel sem semper gravida. Ut posuere.</p>
									</div>
									<div className="col-md-6 col-lg-4 content-grid-item flex-column p-5">
											<img src="images/demos/seo/icons/icon-2.png" className="img-fluid mb-4" alt="" />
											<h3 className="font-weight-semibold text-transform-none text-5 mb-3">App mobile</h3>
											<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor vel sem semper gravida. Ut posuere.</p>
									</div>
									<div className="col-md-6 col-lg-4 content-grid-item flex-column p-5">
											<img src="images/demos/seo/icons/icon-3.png" className="img-fluid mb-4" alt="" />
											<h3 className="font-weight-semibold text-transform-none text-5 mb-3">Seo</h3>
											<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor vel sem semper gravida. Ut posuere.</p>
									</div>
									<div className="col-md-6 col-lg-4 content-grid-item flex-column p-5">
											<img src="images/demos/seo/icons/icon-4.png" className="img-fluid mb-4" alt="" />
											<h3 className="font-weight-semibold text-transform-none text-5 mb-3">Digital Marketing</h3>
											<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor vel sem semper gravida. Ut posuere.</p>
									</div>
									<div className="col-md-6 col-lg-4 content-grid-item flex-column p-5">
											<img src="images/demos/seo/icons/icon-5.png" className="img-fluid mb-4" alt="" />
											<h3 className="font-weight-semibold text-transform-none text-5 mb-3">Social Media Marketing</h3>
											<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor vel sem semper gravida. Ut posuere.</p>
									</div>
									<div className="col-md-6 col-lg-4 content-grid-item flex-column p-5">
											<img src="images/demos/seo/icons/icon-6.png" className="img-fluid mb-4" alt="" />
											<h3 className="font-weight-semibold text-transform-none text-5 mb-3">Link Building</h3>
											<p className="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget tortor vel sem semper gravida. Ut posuere.</p>
									</div>
							</div>
					</div>
					<div className="row mt-5">
							<div className="col text-center">
									<a href="#demo-form" className="btn btn-primary btn-outline btn-rounded popup-with-form font-weight-semibold text-4 btn-px-5 btn-py-2">Get a Quote</a>
							</div>
					</div>
			</div>	
			
        )
    }
}
export default Solutions