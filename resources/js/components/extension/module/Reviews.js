import axios from 'axios'
import React , { Component } from 'react';

class Reviews extends Component {
    render () {
        return (
			

			<section className="section section-height-3 border-0 m-0 appear-animation" data-appear-animation="fadeIn" id="reviews">
					<div className="container">
						<div className="row">
							<div className="col text-center">
								<h2 className="font-weight-bold line-height-2 text-7 mb-1">Reviews</h2>
								<span className="d-block text-color-dark text-5 pb-2 mb-4 opacity-7">Testimonials</span>
							</div>
						</div>
						<div className="row justify-content-center">
							<div className="col-md-11 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="600">
								<div className="owl-carousel owl-theme nav-bottom rounded-nav mb-0" data-plugin-options="{'items': 1, 'loop': false}">
									<div>
										<div className="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
											<div className="testimonial-author mb-0">
												<img src="images/clients/client-1.jpg" className="img-fluid rounded-circle custom-mw-1 mb-1" alt=""/>
											</div>
											<blockquote>
												<p className="text-color-dark opacity-7 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce ante tellus, convallis non consectetur sed, pharetra nec ex.</p>
											</blockquote>
											<div className="testimonial-author mb-3">
												<p><strong className="font-weight-bold">John Smith</strong><span className="font-weight-normal">CEO & Founder - Okler</span></p>
											</div>
										</div>
									</div>
									<div>
										<div className="testimonial testimonial-style-2 testimonial-with-quotes mb-0">
											<div className="testimonial-author mb-0">
												<img src="images/clients/client-2.jpg" className="img-fluid rounded-circle custom-mw-1 mb-1" alt=""/>
											</div>
											<blockquote>
												<p className="text-color-dark opacity-7 mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
											</blockquote>
											<div className="testimonial-author">
												<p><strong className="font-weight-bold">Jessica Smith</strong><span className="font-weight-normal">Marketing - Okler</span></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			
        )
    }
}
export default Reviews