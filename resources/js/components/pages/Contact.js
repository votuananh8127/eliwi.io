import axios from 'axios'
import React, { Component } from 'react'

class Contact extends Component {


  render() {
    return (

<div>
      <div role="main" className="main">

			
				<div id="googlemaps" className="google-map mt-0"></div>

				<div className="container">

					<div className="row py-4">
						<div className="col-lg-6">

							<div className="overflow-hidden mb-1">
								<h2 className="font-weight-normal text-7 mt-2 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200"><strong className="font-weight-extra-bold">Contact</strong> Us</h2>
							</div>
							<div className="overflow-hidden mb-4 pb-3">
								<p className="mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">Feel free to ask for details, don't save any questions!</p>
							</div>

							<form id="contactForm" className="contact-form" action="" method="POST">
								<div className="contact-form-success alert alert-success d-none mt-4" id="contactSuccess">
									<strong>Success!</strong> Your message has been sent to us.
								</div>
							
								<div className="contact-form-error alert alert-danger d-none mt-4" id="contactError">
									<strong>Error!</strong> There was an error sending your message.
									<span className="mail-error-message text-1 d-block" id="mailErrorMessage"></span>
								</div>
								
								<div className="form-row">
									<div className="form-group col-lg-6">
										<label className="required font-weight-bold text-dark text-2">Full Name</label>
										<input type="text" data-msg-required="Please enter your name."  className="form-control" name="name" id="name" required/>
									</div>
									<div className="form-group col-lg-6">
										<label className="required font-weight-bold text-dark text-2">Email Address</label>
										<input type="email" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address."  className="form-control" name="email" id="email" required/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group col">
										<label className="font-weight-bold text-dark text-2">Subject</label>
										<input type="text" data-msg-required="Please enter the subject." className="form-control" name="subject" id="subject" required/>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group col">
										<label className="required font-weight-bold text-dark text-2">Message</label>
										<textarea  data-msg-required="Please enter your message." rows="8" className="form-control" name="message" id="message" required></textarea>
									</div>
								</div>
								<div className="form-row">
									<div className="form-group col">
										<input type="submit" value="Send Message" className="btn btn-primary btn-modern" data-loading-text="Loading..."/>
									</div>
								</div>
							</form>

						</div>
						<div className="col-lg-6">

							<div className="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
								<h4 className="mt-2 mb-1">Our <strong>Office</strong></h4>
								<ul className="list list-icons list-icons-style-2 mt-2">
									<li><i className="fas fa-map-marker-alt top-6"></i> <strong className="text-dark">Address:</strong> 1234 Street Name, City Name, United States</li>
									<li><i className="fas fa-phone top-6"></i> <strong className="text-dark">Phone:</strong> (123) 456-789</li>
									<li><i className="fas fa-envelope top-6"></i> <strong className="text-dark">Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
								</ul>
							</div>

							<div className="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
								<h4 className="pt-5">Business <strong>Hours</strong></h4>
								<ul className="list list-icons list-dark mt-2">
									<li><i className="far fa-clock top-6"></i> Monday - Friday - 9am to 5pm</li>
									<li><i className="far fa-clock top-6"></i> Saturday - 9am to 2pm</li>
									<li><i className="far fa-clock top-6"></i> Sunday - Closed</li>
								</ul>
							</div>

							<h4 className="pt-5">Get in <strong>Touch</strong></h4>
							<p className="lead mb-0 text-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

						</div>

					</div>

				</div>

			</div>

			<section className="call-to-action call-to-action-default with-button-arrow content-align-center call-to-action-in-footer">
				<div className="container">
					<div className="row">
						<div className="col-md-9 col-lg-9">
							<div className="call-to-action-content">
								<h2 className="font-weight-normal text-6 mb-0">Porto is <strong className="font-weight-extra-bold">everything</strong> you need to create an <strong className="font-weight-extra-bold">awesome</strong> website!</h2>
								<p className="mb-0">The <strong className="font-weight-extra-bold">Best</strong> HTML Site Template on ThemeForest</p>
							</div>
						</div>
						<div className="col-md-3 col-lg-3">
							<div className="call-to-action-btn">
								<a href="" target="_blank" className="btn btn-dark btn-lg text-3 font-weight-semibold px-4 py-3">Get Started Now</a>
                <span className="arrow hlb d-none d-md-block" data-appear-animation="rotateInUpLeft"></span>
							</div>
						</div>
					</div>
				</div>
			</section>
  </div>




        )
      }
    }
    
export default Contact