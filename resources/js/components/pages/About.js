import axios from 'axios'
import React, { Component } from 'react'

class About extends Component {


  render() {
    return (


      <div role="main" className="main">

				<div className="container pt-5">

					<div className="row py-4 mb-2">
						<div className="col-md-7 order-2">
							<div className="overflow-hidden">
								<h2 className="text-color-dark font-weight-bold text-8 mb-0 pt-0 mt-0" data-appear-animation="maskUp" data-appear-animation-delay="300">Alice Doe</h2>
							</div>
							<div className="overflow-hidden mb-3">
								<p className="font-weight-bold text-primary text-uppercase mb-0" data-appear-animation="maskUp" data-appear-animation-delay="500">Marketing</p>
							</div>
							<p className="lead" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="700">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam <a href="#">vehicula</a> sit amet enim ac sagittis. Curabitur eget leo varius, elementum mauris eget, egestas quam. Donec ante risus, dapibus sed lectus non, lacinia vestibulum nisi. Morbi vitae augue quam. Nullam ac laoreet libero.</p>
							<p className="pb-3" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">Consectetur adipiscing elit. Aliquam iaculis sit amet enim ac sagittis. Curabitur eget leo varius, elementum mauris eget, egestas quam.</p>
							<hr className="solid my-4" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="900"/>
							<div className="row align-items-center" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1000">
								<div className="col-lg-6">
									<a href="#" className="btn btn-modern btn-dark mt-3">Get In Touch</a>
									<a href="#" className="btn btn-modern btn-primary mt-3">Hire Me</a>
								</div>
								<div className="col-sm-6 text-lg-right my-4 my-lg-0">
									<strong className="text-uppercase text-1 mr-3 text-dark">follow me</strong>
									<ul className="social-icons float-lg-right">
										<li className="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i className="fab fa-facebook-f"></i></a></li>
										<li className="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i className="fab fa-twitter"></i></a></li>
										<li className="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i className="fab fa-linkedin-in"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
						<div className="col-md-5 order-md-2 mb-4 mb-lg-0" data-appear-animation="fadeInRightShorter">
							<img src="images/team/team-7.jpg" className="img-fluid mb-2" alt=""/>
						</div>
					</div>
				</div>

				<section className="section section-default border-0 mt-5" data-appear-animation="fadeIn" data-appear-animation-delay="1200">
					<div className="container py-4">

						<div className="row featured-boxes featured-boxes-style-4">
							<div className="col-md-6 col-lg-3 my-2">
								<div className="m-0 featured-box featured-box-primary featured-box-effect-4" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1600">
									<div className="box-content p-0 text-left">
										<i className="icon-featured icon-screen-tablet icons text-12 m-0 p-0"></i>
										<h4 className="font-weight-bold text-color-dark">Mobile Apps</h4>
										<p className="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
									</div>
								</div>
							</div>
							<div className="col-md-6 col-lg-3 my-2">
								<div className="m-0 featured-box featured-box-primary featured-box-effect-4" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1400">
									<div className="box-content p-0 text-left">
										<i className="icon-featured icon-layers icons text-12 m-0 p-0"></i>
										<h4 className="font-weight-bold text-color-dark">Creative Websites</h4>
										<p className="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
									</div>
								</div>
							</div>
							<div className="col-md-6 col-lg-3 my-2">
								<div className="m-0 featured-box featured-box-primary featured-box-effect-4" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="1400">
									<div className="box-content p-0 text-left">
										<i className="icon-featured icon-magnifier icons text-12 m-0 p-0"></i>
										<h4 className="font-weight-bold text-color-dark">SEO Optimization</h4>
										<p className="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
									</div>
								</div>
							</div>
							<div className="col-md-6 col-lg-3 my-2">
								<div className="m-0 featured-box featured-box-primary featured-box-effect-4" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="1600">
									<div className="box-content p-0 text-left">
										<i className="icon-featured icon-screen-desktop icons text-12 m-0 p-0"></i>
										<h4 className="font-weight-bold text-color-dark">Brand Solutions</h4>
										<p className="mb-0">Lorem ipsum dolor sit amet, coctetur adipiscing elit.</p>
									</div>
								</div>
							</div>
						</div>

					</div>
				</section>

				<div className="container pt-5 pb-2">
					<div className="overflow-hidden">
						<h2 className="text-color-dark font-weight-normal text-6 mb-0" data-appear-animation="maskUp"><strong className="font-weight-extra-bold">Projects</strong></h2>
					</div>
					<div className="overflow-hidden mb-3">
						<p className="mb-0" data-appear-animation="maskUp" data-appear-animation-delay="200">Lorem ipsum dolor sit amet, consectetur adipiscing elit massa enim. Nullam id varius nunc.</p>
					</div>
					<div className="row">
						<div className="col">
							
							<div className="my-4 lightbox" data-appear-animation="fadeInUpShorter" data-plugin-options="{'delegate': 'a.lightbox-portfolio', 'type': 'image', 'gallery': {'enabled': true}}">
								<div className="owl-carousel owl-theme pb-3" data-plugin-options="{'items': 4, 'margin': 35, 'loop': false}">
					

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Presentation</span>
													<span className="thumb-info-type">Brand</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-1.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Porto Watch</span>
													<span className="thumb-info-type">Media</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-1.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-2.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Identity</span>
													<span className="thumb-info-type">Logo</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-2.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-27.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Porto Screens</span>
													<span className="thumb-info-type">Website</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-27.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-4.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Three Bottles</span>
													<span className="thumb-info-type">Logo</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-4.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-5.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Company T-Shirt</span>
													<span className="thumb-info-type">Brand</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-5.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-6.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Mobile Mockup</span>
													<span className="thumb-info-type">Website</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-6.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-7.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Porto Label</span>
													<span className="thumb-info-type">Media</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-7.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-23.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Business Folders</span>
													<span className="thumb-info-type">Logo</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-23.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-24.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Tablet Screen</span>
													<span className="thumb-info-type">Website</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-24.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-25.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Black Watch</span>
													<span className="thumb-info-type">Media</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-25.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>

									<div className="portfolio-item">
										<span className="thumb-info thumb-info-lighten thumb-info-no-borders thumb-info-bottom-info thumb-info-centered-icons border-radius-0">
											<span className="thumb-info-wrapper border-radius-0">
												<img src="images/projects/project-26.jpg" className="img-fluid border-radius-0" alt=""/>
												<span className="thumb-info-title">
													<span className="thumb-info-inner line-height-1 font-weight-bold text-dark position-relative top-3">Monitor Mockup</span>
													<span className="thumb-info-type">Website</span>
												</span>
												<span className="thumb-info-action">
													<a href="portfolio-single-wide-slider.html">
														<span className="thumb-info-action-icon thumb-info-action-icon-primary"><i className="fas fa-link"></i></span>
													</a>
													<a href="images/projects/project-26.jpg" className="lightbox-portfolio">
														<span className="thumb-info-action-icon thumb-info-action-icon-light"><i className="fas fa-search text-dark"></i></span>
													</a>
												</span>
											</span>
										</span>
									</div>
					
								</div>
							</div>
					

					
						</div>
					</div>
				</div>

			</div>






        )
      }
    }
    
export default About