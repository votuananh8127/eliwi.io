import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import ProjectList from './ProjectList';
import NewProject from './NewProject';
//import SingleProject from './SingleProject';
import Slide from './Slide';
import Contact from './pages/Contact';
import About from './pages/About';
import Footer from './common/Footer';
import Solution from './pages/Solution';
import Newletter from './extension/module/Newletter';
import Abouts from './extension/module/Abouts';
import Solutions from './extension/module/Solutions';
import Reviews from './extension/module/Reviews';
import News from './extension/module/News';
import Manufacturer from './extension/module/Manufacturer';
import Portfolio from './extension/module/Portfolio';
export default class App extends Component {
	render() {
		return (
			<BrowserRouter>
					<Header />
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<Slide />
							);
						}} />
					</Switch>
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<Newletter />
							);
						}} />
					</Switch>
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<Abouts />
							);
						}} />
					</Switch>
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<Solutions />
							);
						}} />
					</Switch>
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<Reviews />
								
							);
						}} />
					</Switch>
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<News />
								
							);
						}} />
					</Switch>
					<Switch>
						<Route exact path="/" render={() => {
							return (
								<Manufacturer />
								
							);
						}} />
					</Switch>
					<Route path='/contact.html' component={Contact} />
					<Route path='/about.html' component={About} />
					<Route path='/solution.html' component={Solution} />
					<Route path='/' component={Footer} />
					
					{/* <Switch> */}
						{/* <Route exact path='/' component={ProjectList} /> */}
						{/* <Route path='/create' component={NewProject} /> */}
						{/* <Route path='/:id' component={SingleProject} /> */}
						{/* <Route path='/contact' component={Contact} /> */}
					{/* </Switch> */}
				

			</BrowserRouter>
		);
	}
}

if (document.getElementById('app')) {
	ReactDOM.render(<App />, document.getElementById('app'));
}
