import React from 'react';
import { Link } from 'react-router-dom';


const Menu = () => (

    <div className="header-column justify-content-end">
                                        <div className="header-row">
                                                <div className="header-nav header-nav-links justify-content-start order-2 order-lg-1">
                                                        <div className="header-nav-main header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                                                <nav className="collapse">
                                                                        <ul className="nav nav-pills" id="mainNav">
                                                                                <li className="dropdown">
																				<Link className="dropdown-item active" to="/">Home</Link>
                                                                                </li>
                                                                                <li className="dropdown">
																					<Link className="dropdown-item" to="/about.html">About</Link>
                                                                                </li>
                                                                                <li className="dropdown">
																				<Link className="dropdown-item" to="/solution.html">Solutions</Link>
                                                                                </li>
                                                                                <li className="dropdown">
																				<Link className="dropdown-item" to="/solution.html">Pricing</Link>
                                                                                </li>
                                                                                <li className="dropdown">
                                                                                        <a className="dropdown-item"  data-hash data-hash-offset="95">Reviews</a>
                                                                                </li>
                                                                                <li className="dropdown">
																					<Link className="dropdown-item" to="/contact.html" >Contact</Link>
                                                                                </li>
                                                                        </ul>
                                                                </nav>
                                                        </div>
                                                        <button className="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                                                <i className="fas fa-bars"></i>
                                                        </button>
                                                </div>
                                                <a className="btn btn-primary btn-rounded font-weight-semibold text-3 btn-px-5 btn-py-2 order-1 order-lg-2 d-none d-md-block mr-3 mr-lg-0" data-hash data-hash-offset="65" href="#freeseo">Free SEO Analysis</a>
                                        </div>
                                </div>




)

export default Menu