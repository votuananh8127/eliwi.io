<!DOCTYPE html>
    <html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Tasksman</title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<!-- Vendor CSS -->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}"/>
		<link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}"/>
		<link rel="stylesheet" href="{{ asset('vendor/animate/animate.min.css') }}"/>
		<link rel="stylesheet" href="{{ asset('vendor/simple-line-icons/css/simple-line-icons.min.css') }}"/>
		<link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.carousel.min.css') }}"/>
		<link rel="stylesheet" href="{{ asset('vendor/owl.carousel/assets/owl.theme.default.min.css') }}"/>
		<link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.min.css') }}"/>

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('css/theme/theme.css') }}">
		<link rel="stylesheet" href="{{ asset('css/theme/theme-elements.css') }}">
		<link rel="stylesheet" href="{{ asset('css/theme/theme-blog.css') }}">
		<link rel="stylesheet" href="{{ asset('css/theme/theme-shop.css') }}">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/settings.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/layers.css') }}">
		<link rel="stylesheet" href="{{ asset('vendor/rs-plugin/css/navigation.css') }}">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="{{ asset('css/theme/demos/demo-seo.css') }}">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('css/theme/skins/skin-seo.css') }}"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('css/theme/custom.css') }}">

		<!-- Head Libs -->
		<script src="{{ asset('vendor/modernizr/modernizr.min.js') }}"></script>
    </head>
    <body class="royal_preloader">
    <div id="royal_preloader"></div>
       
        

        <div id="app"></div>

        <script src="{{ asset('js/app.js') }}"></script>





        <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.appear/jquery.appear.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.easing/jquery.easing.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.cookie/jquery.cookie.min.js') }}"></script>
		<script src="{{ asset('vendor/popper/umd/popper.min.js') }}"></script>
		<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('vendor/common/common.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.validation/jquery.validate.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.gmap/jquery.gmap.min.js') }}"></script>
		<script src="{{ asset('vendor/jquery.lazyload/jquery.lazyload.min.js') }}"></script>
		<script src="{{ asset('vendor/isotope/jquery.isotope.min.js') }}"></script>
		<script src="{{ asset('vendor/owl.carousel/owl.carousel.min.js') }}"></script>
		<script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
		<script src="{{ asset('vendor/vide/jquery.vide.min.js') }}"></script>
		<script src="{{ asset('vendor/vivus/vivus.min.js') }}"></script>
		<!-- <script src="{{ asset('vendor/kute/kute.min.js') }}"></script> -->
		<!-- <script src="{{ asset('vendor/kute/kute-svg.min.js') }}"></script> -->
		<script src="{{ asset('js/theme/theme.js') }}"></script>
		<script src="{{ asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
		<script src="{{ asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
		<!-- <script src="{{ asset('js/theme/demos/demo-seo.js') }}"></script> -->
		<script src="{{ asset('js/theme/custom.js') }}"></script>
		<script src="{{ asset('js/theme/theme.init.js') }}"></script>

    </body>
    </html>